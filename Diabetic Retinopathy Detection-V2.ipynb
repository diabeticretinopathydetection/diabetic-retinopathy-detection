{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Diabetic Retinopathy Detection\n",
    "\n",
    "--------\n",
    "\n",
    "#### Rafael Caldeira Silva,  André Trofino\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introdução (from Kaggle)\n",
    "\n",
    "<img src=\"files/retina.jpg\"> Diabetic retinopathy is the leading cause of blindness in the working-age population of the developed world. It is estimated to affect over 93 million people.\n",
    "\n",
    "The US Center for Disease Control and Prevention estimates that 29.1 million people in the US have diabetes and the World Health Organization estimates that 347 million people have the disease worldwide. Diabetic Retinopathy (DR) is an eye disease associated with long-standing diabetes. Around 40% to 45% of Americans with diabetes have some stage of the disease. Progression to vision impairment can be slowed or averted if DR is detected in time, however this can be difficult as the disease often shows few symptoms until it is too late to provide effective treatment.\n",
    "\n",
    "Currently, detecting DR is a time-consuming and manual process that requires a trained clinician to examine and evaluate digital color fundus photographs of the retina. By the time human readers submit their reviews, often a day or two later, the delayed results lead to lost follow up, miscommunication, and delayed treatment.\n",
    "\n",
    "Clinicians can identify DR by the presence of lesions associated with the vascular abnormalities caused by the disease. While this approach is effective, its resource demands are high. The expertise and equipment required are often lacking in areas where the rate of diabetes in local populations is high and DR detection is most needed. As the number of individuals with diabetes continues to grow, the infrastructure needed to prevent blindness due to DR will become even more insufficient.\n",
    "\n",
    "The need for a comprehensive and automated method of DR screening has long been recognized, and previous efforts have made good progress using image classification, pattern recognition, and machine learning. With color fundus photography as input, the goal of this competition is to push an automated detection system to the limit of what is possible – ideally resulting in models with realistic clinical potential. The winning models will be open sourced to maximize the impact such a model can have on improving DR detection."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Understanding our dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "from random import randrange\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.pyplot import imread\n",
    "from scipy.misc import imresize\n",
    "\n",
    "# supress scipy imresize deprecation warning\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')\n",
    "\n",
    "import copy\n",
    "import time\n",
    "import shutil\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset_dir = \"/data/datasets/diabeticRetinopathy/\"\n",
    "train_dir = dataset_dir + \"train/\"\n",
    "test_dir = dataset_dir + \"test/\"\n",
    "!du -sch /data/datasets/diabeticRetinopathy/* | sort -h\n",
    "!find /data/datasets/diabeticRetinopathy/train  -type f | wc -l\n",
    "!find /data/datasets/diabeticRetinopathy/test   -type f | wc -l"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_train = pd.read_csv(train_dir + \"trainLabels.csv\")\n",
    "print(\"{} entries from {} patients in train set\".format(df_train.shape[0], int(df_train.shape[0]/2)))\n",
    "df_train.head(6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(df_train['level'].value_counts())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import seaborn as sns\n",
    "sns.countplot(x='level',data=df_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "images = df_train['image'].apply(lambda img: img + '.jpeg')\n",
    "labels = df_train['level']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "def image_file_path(folder, image_name):\n",
    "    return os.path.join(folder, image_name)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10,10))\n",
    "fig.subplots_adjust(left=0, right=1, bottom=0, top=1, hspace=0.05, wspace=0.05)\n",
    "for i in range(6):\n",
    "    index = randrange(0, images.size)\n",
    "    img = imread(image_file_path(train_dir, images[index]))\n",
    "    img_plot = plt.subplot(161 + i)\n",
    "    img_plot.yaxis.set_visible(False)\n",
    "    img_plot.set_title(labels[index])\n",
    "    img_plot.imshow(img)\n",
    "    \n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Algumas características importantes\n",
    "\n",
    " * As imagens têm tamanhos e estilos de cores variados (esperado, pela descrição do Kaggle)\n",
    " * As classes não estão bem balanceadas\n",
    " * O mesmo paciente pode ter níveis diferentes em cada olho (e.g. 15_left e 15_right)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Manage Dataset and normalizations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "import torch.nn as nn\n",
    "import torch.nn.functional as F\n",
    "import torch.optim as optim\n",
    "from torch.optim.lr_scheduler import ReduceLROnPlateau\n",
    "from torch.autograd import Variable\n",
    "from torch.utils.data import Dataset, DataLoader, TensorDataset\n",
    "from torch.optim.lr_scheduler import StepLR\n",
    "\n",
    "from torchvision import datasets, models\n",
    "import torchvision.transforms as transforms"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Rescale(object):\n",
    "    \"\"\"\n",
    "    Rescale the sample to a given size. Args:\n",
    "        output_size (tuple or int): Desired output size. \n",
    "            If tuple, output is matched to output_size.\n",
    "            If int, smaller of image edges is matched to output_size keeping aspect ratio the same.\n",
    "    \"\"\"\n",
    "\n",
    "    def __init__(self, output_size):\n",
    "        assert isinstance(output_size, (int, tuple))\n",
    "        self.output_size = output_size\n",
    "            \n",
    "    def __call__(self, sample):\n",
    "        sample['image'] = self.resizeImage(sample['image'])\n",
    "        \n",
    "        return sample\n",
    "    \n",
    "    def resizeImage(self, image):\n",
    "        h, w = image.shape[:2]\n",
    "        if isinstance(self.output_size, int):  \n",
    "            if h > w:\n",
    "                new_h, new_w = self.output_size * h / w, self.output_size\n",
    "            else:\n",
    "                new_h, new_w = self.output_size, self.output_size * w / h\n",
    "        else:\n",
    "            new_h, new_w = self.output_size\n",
    "\n",
    "        new_h, new_w = int(new_h), int(new_w)\n",
    "\n",
    "        image = imresize(image, (new_h, new_w))\n",
    "        return image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class CropSquare(object):\n",
    "    \"\"\"\n",
    "    Crop the sample images to the biggest centralized square in the original image\n",
    "    \"\"\"     \n",
    "    def __call__(self, sample):\n",
    "        sample['image'] = self.cropSquare(sample['image'])\n",
    "    \n",
    "        return sample\n",
    "    \n",
    "    def cropSquare(self, image):\n",
    "        # print(image.shape[:2])\n",
    "        h, w = image.shape[:2]\n",
    "        half_cut = abs(w-h)//2\n",
    "        # print(half_cut)\n",
    "        \n",
    "        if h > w:\n",
    "            image = image[half_cut:half_cut+w,:]\n",
    "        else:\n",
    "            image = image[:,half_cut:half_cut+h]\n",
    "        \n",
    "        return image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class ToTensor(object):\n",
    "    \"\"\"Convert ndarrays in sample to Tensors.\"\"\"\n",
    "\n",
    "    def __call__(self, sample):\n",
    "        image = sample['image']\n",
    "        label = sample['label']\n",
    "        # swap color axis because\n",
    "        # numpy image: H x W x C\n",
    "        # torch image: C X H X W\n",
    "        image = image.transpose((2, 0, 1))\n",
    "        \n",
    "        return {\n",
    "            'image' : torch.from_numpy(image).float(),\n",
    "            'label' : label# torch.from_numpy(np.asarray(sample['label'])).long()\n",
    "        }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Normalize(object):\n",
    "    def __call__(self, sample):\n",
    "        normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],\n",
    "                                         std=[0.229, 0.224, 0.225])\n",
    "        \n",
    "        sample['image'] = sample['image']/255.0 # normalizing to [0,1[\n",
    "        \n",
    "        sample['image'] = normalize(sample['image'] / 255.0) # reseting to the expected mean of VGG\n",
    "        \n",
    "        return sample"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class imageDataset(Dataset):\n",
    "    \n",
    "    def __init__(self, dataset_dir, labels_csv, transform=None, n_samples=0, all_classes=False):\n",
    "        self.dir = dataset_dir\n",
    "        self.transform = transform\n",
    "        \n",
    "        self.df = pd.read_csv(labels_csv)\n",
    "\n",
    "        self.num_classes = len(self.df['level'].unique())\n",
    "        \n",
    "        self._balance_samples(n_samples, all_classes)\n",
    "\n",
    "    def __len__(self):\n",
    "        return self.df.shape[0]\n",
    "\n",
    "    def __getitem__(self, i):\n",
    "        \n",
    "        if isinstance(i, slice):\n",
    "            start = 0 if not i.start else i.start\n",
    "            stop = len(self) if not i.stop else i.stop\n",
    "            step = 1 if not i.step else i.step\n",
    "            \n",
    "            return [self[i] for i in range(start, stop, step)]\n",
    "            \n",
    "        else:\n",
    "            \n",
    "            image  = imread(os.path.join(self.dir, self.df.iloc[i]['image'] + '.png'))\n",
    "            label  = self.df.iloc[i]['level']\n",
    "            \n",
    "            # One Hot\n",
    "            # label = np.eye(self.num_classes)[label]\n",
    "            \n",
    "            sample = {\n",
    "                'image' : image,\n",
    "                'label' : label,\n",
    "            }\n",
    "            if self.transform:\n",
    "                # print(\"transforming data\")\n",
    "                sample = self.transform(sample)\n",
    "            return sample\n",
    "    \n",
    "    def _balance_samples(self, n, all_classes):\n",
    "        if n > 0:\n",
    "            if all_classes:\n",
    "                class_n = int(n / self.num_classes)\n",
    "\n",
    "                level_0_df = self.df.query('level == 0').sample(class_n)\n",
    "                level_1_df = self.df.query('level == 1').sample(class_n)\n",
    "                level_2_df = self.df.query('level == 2').sample(class_n)\n",
    "                level_3_df = self.df.query('level == 3').sample(class_n)\n",
    "                level_4_df = self.df.query('level == 4').sample(class_n)\n",
    "\n",
    "                self.df = pd.concat([level_0_df, level_1_df, level_2_df, level_3_df, level_4_df])\n",
    "                # Shuffle and reset index\n",
    "                self.df = self.df.sample(frac=1).reset_index(drop=True)\n",
    "                \n",
    "            else:\n",
    "                self.num_classes = 2\n",
    "                class_n = int(n / 2)\n",
    "                \n",
    "                level_0_df = self.df.query('level == 0').sample(class_n)\n",
    "                level_1_df = self.df.query('level == 1 or level == 2 or level == 3 or level == 4').sample(class_n)\n",
    "                \n",
    "                # Replace all labels for 1\n",
    "                level_1_df.loc[level_1_df.level > 0, 'level'] = 1\n",
    "            \n",
    "                self.df = pd.concat([level_0_df, level_1_df])\n",
    "                # Shuffle and reset index\n",
    "                self.df = self.df.sample(frac=1).reset_index(drop=True)\n",
    "            \n",
    "    def split_validation(self, split):\n",
    "        int_split = int(np.round(self.df.shape[0] * split))\n",
    "        \n",
    "        validation = copy.deepcopy(self)\n",
    "        validation.df = validation.df[int_split:]\n",
    "        \n",
    "        self.df = self.df[:int_split]\n",
    "        \n",
    "        return validation\n",
    "        "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "composed = transforms.Compose([ToTensor(),\n",
    "                               Normalize()])\n",
    "\n",
    "n_samples = 14000\n",
    "split = 0.8\n",
    "\n",
    "train_dir = \"/data/datasets/diabeticRetinopathy/reduced_train_224/\"\n",
    "labels_csv = \"/data/datasets/diabeticRetinopathy/train/trainLabels.csv\"\n",
    "\n",
    "train_dataset = imageDataset(train_dir, labels_csv, composed, n_samples=n_samples, all_classes=False)\n",
    "val_dataset = train_dataset.split_validation(split)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_loader = DataLoader(\n",
    "    train_dataset, \n",
    "    batch_size=10, \n",
    "    shuffle=True, \n",
    "    num_workers=8,\n",
    "    pin_memory=True\n",
    ")\n",
    "\n",
    "val_loader = DataLoader(\n",
    "    val_dataset, \n",
    "    batch_size=10, \n",
    "    shuffle=False, \n",
    "    num_workers=2,\n",
    "    pin_memory=True\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# test datasets and data loader\n",
    "print(\"{} items for train and {} for validation\".format(len(train_dataset), len(val_dataset)))\n",
    "\n",
    "data, label = train_loader.sampler.data_source[0].values()\n",
    "print(data.max(), data.min())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MyDenseAux(torch.nn.Module):\n",
    "    def __init__(self):\n",
    "        super(MyDenseAux, self).__init__()\n",
    "        \n",
    "        self.dense1 = torch.nn.Linear(512 * 7 * 7, 1024)\n",
    "        self.drop1 =  torch.nn.Dropout(0.5)\n",
    "        self.dense2 = torch.nn.Linear(1024, 128)\n",
    "        self.drop2 =  torch.nn.Dropout(0.5)\n",
    "        self.dense3 = torch.nn.Linear(128, 32)\n",
    "        \n",
    "    def forward(self, x):\n",
    "        print(x.size())\n",
    "        x = x.view(-1, 512 * 7 * 7)\n",
    "        x = F.relu(self.dense1(x))\n",
    "        x = self.drop1(x)\n",
    "        x = F.relu(self.dense2(x))\n",
    "        x = self.drop2(x)\n",
    "        x = F.relu(self.dense3(x))\n",
    "        return x\n",
    "    \n",
    "class Net(torch.nn.Module):\n",
    "    def __init__(self):\n",
    "        super(Net, self).__init__()\n",
    "        \n",
    "        self.vgg = models.vgg11(pretrained=True)\n",
    "        # self.vgg.features = torch.nn.DataParallel(self.vgg.features)\n",
    "        self.vgg.classifier = nn.Sequential(\n",
    "            torch.nn.Linear(512 * 7 * 7, 1024),\n",
    "            torch.nn.Dropout(0.5),\n",
    "            torch.nn.Linear(1024, 128),\n",
    "            torch.nn.Dropout(0.5),\n",
    "            torch.nn.Linear(128, 32)\n",
    "        )\n",
    "        \n",
    "        for param in self.vgg.features.parameters():\n",
    "            param.requires_grad = False\n",
    "        for param in self.vgg.classifier.parameters():\n",
    "            param.requires_grad = True\n",
    "        \n",
    "        self.finalClassifier = nn.Sequential(\n",
    "            nn.Linear(32, 16),\n",
    "            nn.Dropout(0.5),\n",
    "            nn.Linear(16, 5)\n",
    "        )\n",
    "        \n",
    "    def forward(self, x):\n",
    "        # get vgg output\n",
    "        x = self.vgg(x)\n",
    "        x = x.view(-1, 32)\n",
    "        x = self.finalClassifier(x)\n",
    "        \n",
    "        return x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class FineTuneModel(nn.Module):\n",
    "    def __init__(self, original_model, arch, num_classes):\n",
    "        super(FineTuneModel, self).__init__()\n",
    "\n",
    "        if arch.startswith('alexnet') :\n",
    "            self.features = original_model.features\n",
    "            self.classifier = nn.Sequential(\n",
    "                nn.Dropout(),\n",
    "                nn.Linear(256 * 6 * 6, 4096),\n",
    "                nn.ReLU(inplace=True),\n",
    "                nn.Dropout(),\n",
    "                nn.Linear(4096, 4096),\n",
    "                nn.ReLU(inplace=True),\n",
    "                nn.Linear(4096, num_classes),\n",
    "            )\n",
    "            self.modelName = 'alexnet'\n",
    "        elif arch.startswith('resnet') :\n",
    "            # Everything except the last linear layer\n",
    "            self.features = nn.Sequential(*list(original_model.children())[:-1])\n",
    "            self.classifier = nn.Sequential(\n",
    "                nn.Linear(512, num_classes)\n",
    "            )\n",
    "            self.modelName = 'resnet'\n",
    "        elif arch.startswith('vgg16'):\n",
    "            self.features = original_model.features\n",
    "            self.classifier = nn.Sequential(\n",
    "                nn.Dropout(),\n",
    "                nn.Linear(25088, 4096),\n",
    "                nn.ReLU(inplace=True),\n",
    "                nn.Dropout(),\n",
    "                nn.Linear(4096, 4096),\n",
    "                nn.ReLU(inplace=True),\n",
    "                nn.Linear(4096, num_classes),\n",
    "            )\n",
    "            self.modelName = 'vgg16'\n",
    "        else :\n",
    "            raise(\"Finetuning not supported on this architecture yet\")\n",
    "\n",
    "        # Freeze those weights\n",
    "        for p in self.features.parameters():\n",
    "            p.requires_grad = False\n",
    "\n",
    "\n",
    "    def forward(self, x):\n",
    "        f = self.features(x)\n",
    "        if self.modelName == 'alexnet' :\n",
    "            f = f.view(f.size(0), 256 * 6 * 6)\n",
    "        elif self.modelName == 'vgg16':\n",
    "            f = f.view(f.size(0), -1)\n",
    "        elif self.modelName == 'resnet' :\n",
    "            f = f.view(f.size(0), -1)\n",
    "        y = self.classifier(f)\n",
    "        return y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "arch = 'resnet'\n",
    "original_model = models.resnet18(pretrained=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = FineTuneModel(original_model, arch, 2)\n",
    "# model.features = torch.nn.DataParallel(model.features)\n",
    "model.cuda()\n",
    "print(model)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Parameters\n",
    "start_epoch = 0\n",
    "epochs = 100\n",
    "batch_size = 10\n",
    "weigth_decay = 1e-4\n",
    "learning_rate = 0.001\n",
    "momentum = 0.9\n",
    "print_freq = 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "import lib.pytorch_trainer as ptt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "criterion = nn.CrossEntropyLoss().cuda()\n",
    "\n",
    "optimizer = torch.optim.SGD(\n",
    "    filter(lambda p: p.requires_grad, model.parameters()), # Only finetunable params\n",
    "    learning_rate,\n",
    "    momentum=momentum\n",
    ")\n",
    "\n",
    "cb_chkpt = ptt.ModelCheckpoint(\n",
    "    './checkpoint.pth',\n",
    "    reset=True, \n",
    "    verbose=1\n",
    ")\n",
    "\n",
    "csv_logger = ptt.CSVLogger(\n",
    "    './logging.csv'\n",
    ")\n",
    "\n",
    "scheduler = StepLR(optimizer, step_size=3, gamma=0.75)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "training_parameters = {\n",
    "    'model':         model, \n",
    "    'criterion':     criterion, \n",
    "    'optimizer':     optimizer, \n",
    "    'lr_scheduler':  scheduler,\n",
    "    'callbacks': [\n",
    "        ptt.PrintCallback(),\n",
    "        cb_chkpt,\n",
    "        ptt.PlotCallback()\n",
    "    ]\n",
    "}\n",
    "trainer = ptt.DeepNetTrainer(**training_parameters)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer.fit_loader(epochs, train_loader, val_loader)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_dir = \"/data/datasets/diabeticRetinopathy/test_reduced_224//\"\n",
    "labels_csv = \"/data/datasets/diabeticRetinopathy/test/testLabels.csv\"\n",
    "\n",
    "test_dataset = imageDataset(test_dir, labels_csv, composed, all_classes=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_loader = DataLoader(\n",
    "    test_dataset, \n",
    "    batch_size=10, \n",
    "    shuffle=True, \n",
    "    num_workers=8,\n",
    "    pin_memory=True\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# test datasets and data loader\n",
    "print(\"{} items for train and {} for validation\".format(len(train_dataset), len(val_dataset)))\n",
    "\n",
    "data, label = test_loader.sampler.data_source[0].values()\n",
    "print(data.max(), data.min())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainer.load_state(\"checkpoint.pth\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = trainer.evaluate_loader(test_loader)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def train(train_loader, model, criterion, optimizer, epoch):\n",
    "    batch_time = AverageMeter()\n",
    "    data_time = AverageMeter()\n",
    "    losses = AverageMeter()\n",
    "    top1 = AverageMeter()\n",
    "    top5 = AverageMeter()\n",
    "\n",
    "    # switch to train mode\n",
    "    model.train()\n",
    "\n",
    "    end = time.time()\n",
    "    for i, data in enumerate(train_loader):\n",
    "        # measure data loading time\n",
    "        data_time.update(time.time() - end)\n",
    "        \n",
    "        input, target = data.values()\n",
    "        \n",
    "        target = target.cuda(async=True)\n",
    "        input_var = torch.autograd.Variable(input.cuda())\n",
    "        target_var = torch.autograd.Variable(target.cuda())\n",
    "        \n",
    "        # compute output\n",
    "        output = model(input_var)\n",
    "        loss = criterion(output, target_var)\n",
    "\n",
    "        # measure accuracy and record loss\n",
    "        prec1, prec5 = accuracy(output.data, target, topk=(1, 2))\n",
    "        losses.update(loss.data[0], input.size(0))\n",
    "        top1.update(prec1[0], input.size(0))\n",
    "        top5.update(prec5[0], input.size(0))\n",
    "\n",
    "        # compute gradient and do SGD step\n",
    "        optimizer.zero_grad()\n",
    "        loss.backward()\n",
    "        optimizer.step()\n",
    "\n",
    "        # measure elapsed time\n",
    "        batch_time.update(time.time() - end)\n",
    "        end = time.time()\n",
    "\n",
    "        if i % print_freq == 0:\n",
    "            print('Epoch: [{0}][{1}/{2}]\\t'\n",
    "                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\\t'\n",
    "                  'Data {data_time.val:.3f} ({data_time.avg:.3f})\\t'\n",
    "                  'Loss {loss.val:.4f} ({loss.avg:.4f})\\t'\n",
    "                  'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\\t'\n",
    "                  'Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(\n",
    "                   epoch, i, len(train_loader), batch_time=batch_time,\n",
    "                   data_time=data_time, loss=losses, top1=top1, top5=top5))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": []
   },
   "outputs": [],
   "source": [
    "def validate(val_loader, model, criterion):\n",
    "    batch_time = AverageMeter()\n",
    "    losses = AverageMeter()\n",
    "    top1 = AverageMeter()\n",
    "    top5 = AverageMeter()\n",
    "\n",
    "    # switch to evaluate mode\n",
    "    model.eval()\n",
    "\n",
    "    end = time.time()\n",
    "    for i, data in enumerate(val_loader):\n",
    "        \n",
    "        input, target = data.values()\n",
    "        \n",
    "        target = target.cuda(async=True)\n",
    "        input_var = torch.autograd.Variable(input.cuda(), volatile=True)\n",
    "        target_var = torch.autograd.Variable(target.cuda(), volatile=True)\n",
    "\n",
    "        # compute output\n",
    "        output = model(input_var)\n",
    "        loss = criterion(output, target_var)\n",
    "\n",
    "        # measure accuracy and record loss\n",
    "        prec1, prec5 = accuracy(output.data, target, topk=(1, 2))\n",
    "        losses.update(loss.data[0], input.size(0))\n",
    "        top1.update(prec1[0], input.size(0))\n",
    "        top5.update(prec5[0], input.size(0))\n",
    "\n",
    "        # measure elapsed time\n",
    "        batch_time.update(time.time() - end)\n",
    "        end = time.time()\n",
    "\n",
    "        if i % print_freq == 0:\n",
    "            print('Test: [{0}/{1}]\\t'\n",
    "                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\\t'\n",
    "                  'Loss {loss.val:.4f} ({loss.avg:.4f})\\t'\n",
    "                  'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\\t'\n",
    "                  'Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(\n",
    "                   i, len(val_loader), batch_time=batch_time, loss=losses,\n",
    "                   top1=top1, top5=top5))\n",
    "\n",
    "    print(' * Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f}'\n",
    "          .format(top1=top1, top5=top5))\n",
    "\n",
    "    return top1.avg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):\n",
    "    torch.save(state, filename)\n",
    "    if is_best:\n",
    "        shutil.copyfile(filename, 'model_best.pth.tar')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def adjust_learning_rate(optimizer, epoch, learning_rate):\n",
    "    \"\"\"Sets the learning rate to the initial LR decayed by 10 every 30 epochs\"\"\"\n",
    "    lr = learning_rate * (0.1 ** (epoch // 30))\n",
    "    for param_group in optimizer.state_dict()['param_groups']:\n",
    "        param_group['lr'] = lr\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class AverageMeter(object):\n",
    "    \"\"\"Computes and stores the average and current value\"\"\"\n",
    "    def __init__(self):\n",
    "        self.reset()\n",
    "\n",
    "    def reset(self):\n",
    "        self.val = 0\n",
    "        self.avg = 0\n",
    "        self.sum = 0\n",
    "        self.count = 0\n",
    "\n",
    "    def update(self, val, n=1):\n",
    "        self.val = val\n",
    "        self.sum += val * n\n",
    "        self.count += n\n",
    "        self.avg = self.sum / self.count"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def accuracy(output, target, topk=(1,)):\n",
    "    \"\"\"Computes the precision@k for the specified values of k\"\"\"\n",
    "    maxk = max(topk)\n",
    "    batch_size = target.size(0)\n",
    "    \n",
    "    _, pred = output.topk(maxk, 1, True, True)\n",
    "    pred = pred.t()\n",
    "    \n",
    "    correct = pred.eq(target.view(1, -1).expand_as(pred))\n",
    "    \n",
    "    res = []\n",
    "    for k in topk:\n",
    "        correct_k = correct[:k].view(-1).float().sum(0)\n",
    "        res.append(correct_k.mul_(100.0 / batch_size))\n",
    "    return res"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "best_prec1 = 0\n",
    "    \n",
    "for epoch in range(start_epoch, epochs):\n",
    "    adjust_learning_rate(optimizer, epoch, learning_rate)\n",
    "\n",
    "    # train for one epoch\n",
    "    train(train_loader, model, criterion, optimizer, epoch)\n",
    "\n",
    "    # evaluate on validation set\n",
    "    prec1 = validate(val_loader, model, criterion)\n",
    "\n",
    "    # remember best prec@1 and save checkpoint\n",
    "    is_best = prec1 > best_prec1\n",
    "    best_prec1 = max(prec1, best_prec1)\n",
    "    save_checkpoint({\n",
    "        'epoch': epoch + 1,\n",
    "        'arch': 'vgg',\n",
    "        'state_dict': model.state_dict(),\n",
    "        'best_prec1': best_prec1,\n",
    "    }, is_best)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "l = DataLoader(\n",
    "    imageDataset(\n",
    "        train_dir, train_dir + \"trainLabels.csv\", \n",
    "        composed, \n",
    "        n_samples=5\n",
    "    ), \n",
    "    batch_size=1, \n",
    "    shuffle=True, \n",
    "    num_workers=2\n",
    ")\n",
    "\n",
    "validate(l, model, criterion)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
